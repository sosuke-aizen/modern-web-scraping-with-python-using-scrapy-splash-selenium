

Hi everyone,

Due to some technical issues from Udemy side, some of you could't download the
resources included to lecture 11 & 14\. If that's the case for you please use
the links I included below to download the resources needed for this section:

 **For lecture 11:**

<https://www.dropbox.com/s/bqrcr7a7vln0qwq/css_html_file.zip?dl=0>

 **For lecture 14:**

<https://www.dropbox.com/s/zk2dbqr2b1talms/xpath_html_file.zip?dl=0>

Please note, this article will be removed as soon as Udemy fixes this issue.

Kind regards,

Ahmed.

