

Hello Everyone,

So your challenge is to use the Files Pipeline to download PDF files using
Scrapy from this website ([LINK](http://www.bitsavers.org/pdf/sony/floppy/)).

####  **Challenge Requirements:**

  1. All files must be downloaded with their original filename and extension.

  2. All files must be stored within your project root directory under ' **/full/** '.

  3. The Item class should contain only 3 fields ( **file_urls** , **files** and **filename** )

For better experience please post your project source code under your Github
account so I can review it, if that's not possible you can upload it in the
Q&A area.

Wish you the best,

Kind regards,  
Ahmed

