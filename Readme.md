# Modern Web Scraping with Python using Scrapy Splash Selenium

### 01. Introduction/

- 01.01. Intro to Web Scraping & Scrapy.mp4
- 01.02. Setting up Scrapy the Development Environment (Updated).mp4
- 01.04. Udemy 101 (Please don't skip).mp4
- 01.02.1 Anaconda official website.html
- 01.02.2 Visual Studio Code.html
- 01.03. Add VSCODE to path (Mac users).html
- 01.05. Asking questions.html


### 02. Scrapy Fundamentals/

- 02.01. Scrapy fundamentals PART 1.mp4
- 02.02. Scrapy fundamentals PART 2.mp4
- 02.03. Scrapy fundamentals PART 3.mp4
- 02.04. Scrapy fundamentals PART 4.mp4
- 02.05. Scrapy fundamentals PART 5.mp4
- 02.01.1 Scrapy docummentation.html
- 02.02.1 Target website link.html


### 03. XPath expressions & CSS Selectors/

- 03.02. XPath & CSS Selectors.mp4
- 03.03. CSS Selectors fundamentals.mp4
- 03.04. CSS selectors in theory.mp4
- 03.05. XPath fundamentals.mp4
- 03.06. Navigating using XPath(Going UP).mp4
- 03.07. Navigating using XPath(Going DOWN).mp4
- 03.08. XPath in theory.mp4
- 03.01. Downloadable files.html
- 03.02.1 code.zip
- 03.03.1 CSS Selectors playground.html
- 03.04.1 CSS Selectors for web scraping cheatsheet(modern).pdf
- 03.05.1 code.zip
- 03.05.2 XPath playground.html
- 03.08.1 XPath for web scraping cheatsheet(modern).pdf


### 04. Project 1 Spiders from A to Z/

- 04.01. Worldometers PART 1.mp4
- 04.02. Worldometers PART 2.mp4
- 04.03. Worldometers PART 3.mp4
- 04.04. Worldometers PART 4.mp4
- 04.05. Project source code.html
- 04.06. Exercise.html


### 05. Building Datasets/

- 05.01. Bulding datesets.mp4
- 05 Readme.md


### 06. Project 2 Dealing with Multiple pages/

- 06.02. Setting up the project.mp4
- 06.03. Building the spider.mp4
- 06.04. Dealing with pagination.mp4
- 06.05. Spoofing request headers.mp4
- 06.01. Website URL.html
- 06.02.1 Target website link.html
- 06.06. TinyDeal project source code.html
- 06.07. Exercise 2.html


### 07. Debugging spiders/

- 07.01. Debugging spiders PART 1.mp4
- 07.02. Debugging spiders PART 2.mp4
- 07.01.1 Debugging spiders.html


### 08. Let's take a break/

- 08.01. The whys & whens of web scraping.mp4
- 08.02. Web scraping challenges.mp4
- 08 Readme.md


### 09. Project 3 Build Crawlers using Scrapy/

- 09.01. Crawl spider structure.mp4
- 09.02. The Rule object.mp4
- 09.03. Following links in pagination.mp4
- 09.04. Spoofing request headers.mp4
- 09.02.1 Target website link.html
- 09.05. Project source code.html
- 09.06. Exercise.html


### 10. Splash crash course/

- 10.01. What dilemma splash came to solve.mp4
- 10.02. Setting up Splash (Windows ProEntreprise edition & Mac Os).mp4
- 10.03. Setting up Splash(Windows Home Edition).mp4
- 10.05. Introduction to Splash.mp4
- 10.06. Working with elements.mp4
- 10.07. Spoofing request headers.mp4
- 10.01.1 Splash docummentation.html
- 10.02.1 Docker Desktop.html
- 10.03.1 Docs Docker Toolbox Windows.html
- 10.03.2 Docker Toolbox download page.html
- 10.03.3 Docs Docker Toolbox Mac.html
- 10.04. Setting up Splash (Linux).html
- 10.05.1 Target website link.html
- 10.05.2 Learn LUA in 15 minutes.html


### 11. Project 4 Scraping JavaScript websites using Splash/

- 11.01. Splash incognito mode.mp4
- 11.02. Using Splash with Scrapy.mp4
- 11.03. Parsing (BAD HTML MARKUP).mp4
- 11.01.1 Target website link.html
- 11.02.1 Scrapy-Splash package.html
- 11.04. Project source code.html
- 11.05. Exercise.html


### 12. Project 5 Scraping JavaScript websites using Selenium/

- 12.01. Selenium basics.mp4
- 12.02. ElementNotInteractable Exception.mp4
- 12.03. Selenium with Scrapy.mp4
- 12.04. Selenium Middleware PART 1 (NEW).mp4
- 12.05. Selenium Middleware PART 2 (NEW).mp4
- 12.01.1 Selenium docummentation.html
- 12.04.1 Scrapy Selenium Middleware.html
- 12.05.1 Target website link.html
- 12.06. Project source code.html


### 13. Working with Pipelines/

- 13.01. Pipelines.mp4
- 13.02. Storing data in MongoDB.mp4
- 13.03. Storing data in SQLite3.mp4
- 13.04. Project source code.html


### 14. Scraping APIs (NEW)/

- 14.01. Scraping APIs PART 1.mp4
- 14.02. Scraping APIs PART 2.mp4
- 14.03. Scraping APIs PART 3.mp4
- 14.04. Scraping APIs PART 4.mp4
- 14.05. Scraping APIs PART 5.mp4
- 14.01.1 Target website link.html
- 14.04.1 Target website link.html
- 14.06. Project source code.html


### 15. Log in to websites (NEW)/

- 15.01. Log in to websites PART 1.mp4
- 15.02. Log in to websites PART 2.mp4
- 15.03. Log in to websites PART 3 (JavaScript required).html
- 15.04. Project source code.html


### 16. APPENDIX (OLDER SCRAPY 1.5 CONTENT)/

- 16.01. IMPORTANT.mp4
- 16.02. Avoid getting banned PART 1.mp4
- 16.03. Avoid getting banned PART 2.mp4
- 16.04. Avoid getting banned PART 3.mp4
- 16.05. Scraping APIs PART 1.mp4
- 16.06. Scraping APIs PART 2.mp4
- 16.07. Scraping APIs PART 3.mp4
- 16.08. Scraping APIs PART 4.mp4
- 16.10. Scraping APIs PART 5.mp4
- 16.12. Scraping APIs PART 6.mp4
- 16.13. Spider Arguments.mp4
- 16.14. Scraping APIs PART 7.mp4
- 16.15. IMPORTANT.mp4
- 16.17. Deploying spiders PART 1.mp4
- 16.18. Deploying spiders PART 2.mp4
- 16.19. Deploying spiders PART 3.mp4
- 16.20. Deploying spiders PART 4.mp4
- 16.21. Execute spiders periodically.mp4
- 16.22. Deploy Splash to Heroku.mp4
- 16.23. IMPORTANT.mp4
- 16.27. Login to websites using FormRequest.mp4
- 16.28. XML Http Post Requests.mp4
- 16.32. Media Pipelines.mp4
- 16.33. The Images Pipeline.mp4
- 16.34. Extending The Images Pipeline (Store images with custom names).mp4
- 16.39. Using Crawlera with Scrapy.mp4
- 16.40. Using Crawlera with Splash.mp4
- 16.41. Using Heroku as a Proxy (FREE).mp4
- 16.42. Using FREE Proxies with the CrawlSpider.mp4
- 16.04.1 User Agents List.pdf
- 16.07.1 employees.json
- 16.07.2 test.py
- 16.09. Hidden XHR.html
- 16.10.1 Hidden detail page url.html
- 16.11. IMPORTANT NOTE.html
- 16.14.1 demo_airbnb_new.zip
- 16.16. Another way to scrape Airbnb restaurant detail page.html
- 16.17.1 Scrapy cloud.html
- 16.19.1 .gitignore.html
- 16.21.1 APScheduler documentation.html
- 16.21.2 add_job().pdf
- 16.24. Project source code.html
- 16.25. Project source code.html
- 16.26. Challenge for those who are adventurous.html
- 16.28.1 Target website.html
- 16.29. XML Http Post requests assignment.html
- 16.30. Project source  code.html
- 16.31. Code UPDATE XHR repeated data (Assignment).html
- 16.32.1 Media Pipeline documentation.html
- 16.33.1 Target website.html
- 16.35. IMPORTANT.html
- 16.36. Files Pipeline (Article).html
- 16.37. Challenge (Files Pipeline).html
- 16.38. Project source code.html
- 16.43. IMPORTANT.html
- 16.44. Challenge.html
- 16.45. Project source code.html


### 17. BONUS/

- 17.01. Files Pipeline.html
- 17.02. Bonus Lecture.html